import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:http/http.dart' as http;

void main() {
  return runApp(ChartApp());
}

class ChartApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chart Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<PlotData> chartData = [];

  @override
  void initState() {
    loadSalesData();
    super.initState();
  }

  Future loadSalesData() async {
    final String jsonString = await getJsonFromFirebase();
    var currentJSON = jsonDecode(jsonString)['current'];
    var voltageJSON = jsonDecode(jsonString)['voltage'];
    List<int> current = List.from(currentJSON);
    List<int> voltage = List.from(voltageJSON);
    for (var i =0;i<current.length;i++ )
      chartData.add(PlotData(current[i],voltage[i]));
  }

  Future<String> getJsonFromFirebase() async {
    String url = "http://localhost:8080/PVArrayBasic?array=[45,34,78,43]";
    http.Response response = await http.get(Uri.parse(url));
    print(response.body);
    return response.body;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Syncfusion Flutter chart'),
        ),
        body: Center(
          child: FutureBuilder(
              future: getJsonFromFirebase(),
              builder: (context, snapShot) {
                if (snapShot.hasData) {
                  return SfCartesianChart(
                      primaryXAxis: CategoryAxis(),
                      // Chart title
                      title: ChartTitle(text: 'Current - Voltage'),
                      series: <ChartSeries<PlotData, int>>[
                        LineSeries<PlotData, int>(
                            dataSource: chartData,
                            xValueMapper: (PlotData plot, _) => plot.x,
                            yValueMapper: (PlotData plot, _) => plot.y,
                            // Enable data label
                            dataLabelSettings:
                                DataLabelSettings(isVisible: true))
                      ]);
                } else {
                  return Card(
                    elevation: 5.0,
                    child: Container(
                      height: 100,
                      width: 400,
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text('Retriving Firebase data...',
                                style: TextStyle(fontSize: 20.0)),
                            Container(
                              height: 40,
                              width: 40,
                              child: CircularProgressIndicator(
                                semanticsLabel: 'Retriving Firebase data',
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.blueAccent),
                                backgroundColor: Colors.grey[300],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }
              }),
        ));
  }
}

class PlotData {
  PlotData(this.x, this.y);

  final int x;
  final int y;

  factory PlotData.fromJson(Map<int, dynamic> parsedJson) {
    return PlotData(
      parsedJson['current'],
      parsedJson['voltage'],
    );
  }
}
